# Rotation des backups des bases de données

Voir [le wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:backup:db:rotation).

## Mise à jour

Effectuer les changements désirés dans le Dockerfile, effectuer les changements désirés dans la configuration, mettre à jour le fichier `CHANGELOG.md` et changer la version du service dans le `docker-compose.yml`.

Il n'est pas nécessaire de modifier le numéro de version pour une simple mise à jour de configuration.

## Configuration

Utiliser le fichier `config/backup_data.json`.

Structure :

- Nom du service : Contient une structure de données contenant les informations relatives au service telles qu'indiquées ci-dessous
- `Folder` : Indique le nom du dossier de backup utilisé par le script de backup et de rotation
- `Backup-Rota` : Contient les paramètres pour la rotation des backups dans une structure de données comme indiqué ci-dessous
  - `Hour` : nombre de backups horaires à conserver
  - `Day` : nombre de backups quotidiens à conserver
  - `Week` : nombre de backups hebdomadaires à conserver
  - `Month`: nombre de backups mensuels à conserver



## Lancement

On se synchronise simplement avec le dépôt et on lance le Docker Compose :
```
$ docker-compose up -d
```
