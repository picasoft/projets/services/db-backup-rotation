FROM python:3.8-alpine

RUN pip install --no-cache-dir "rotate-backups<9" "python-crontab<2.5"

COPY start_rotation.py /start_rotation.py

CMD /start_rotation.py && \
    crontab /crontab.conf && \
    crond -f -d 8
